FROM php:7.2-apache
LABEL name="uhi67/sptfademo"
LABEL description="Apache + php7.2"
LABEL version="1.1"
MAINTAINER uhi67 <uherkovich.peter@gmail.com>

# simplesaml (/simplesaml/ Alias)
COPY docker/simplesaml.conf /etc/apache2/conf-available/simplesaml.conf
RUN	chown www-data:www-data /etc/apache2/conf-available/simplesaml.conf \
	&& a2enconf simplesaml

ENV SIMPLESAMLPHP_CONFIG_DIR "/var/www/app/simplesamlphp/config"

# Other apache modules
RUN a2enmod remoteip\
	&& printf 'RemoteIPHeader X-Forwarded-For\n' >> /etc/apache2/apache2.conf
RUN a2enmod rewrite

# Copy application source
COPY . /var/www/app
# symlink to application web dir
RUN (rm -rf /var/www/html && ln -fs /var/www/app/web /var/www/html) || true
# Config from template
COPY config/config-template.php /var/www/app/config/config.php

# write permission for runtime directories
RUN chown -R www-data:www-data /var/www/app/runtime
RUN chown -R www-data:www-data /var/www/app/vendor/simplesamlphp/simplesamlphp/metadata

# custom image initialization
COPY docker/init /usr/local/bin/init
RUN chmod u+x /usr/local/bin/init
COPY docker/insertcert.php /usr/local/bin/insertcert.php
RUN chmod u+x /usr/local/bin/init

# Running environment copy, cron, apache. Apache is the primary process
CMD printenv | sed 's/^\([a-zA-Z0-9_]*\)=\(.*\)$/export \1="\2"/g' > /root/env.sh \
    && /usr/local/bin/init \
    && exec apache2-foreground

# docker build -t uhi67/sptfademo .
