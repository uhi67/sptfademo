<?php
/**
 * Template for manually configured config.php
 * Ready-made version for docker version
 */

use uhi67\helper\EnvHelper;

/** @noinspection PhpUnhandledExceptionInspection */
$authsource = EnvHelper::getEnv('authsource', 'primary');

$config = array(
	/** Configuration for SAML login module */
	'saml' => array(
		'authsource' => $authsource,
	),
);
return $config;
