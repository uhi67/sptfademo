<?php
namespace uhi67\install;

#use Composer\Script\Event;
#use Composer\Installer\PackageEvent;

class Install {
	/** @noinspection PhpUndefinedClassInspection */
	/**
	 * Runned by composer after install/update.
	 * - Creates runtime dir, mdx-cache dir
	 * - Copies private modules and attribute-maps
	 * param Event $event
	 * @throws \Exception
	 */
	public static function postInstall(/*$event*/) {
		#$composer = $event->getComposer();
		$root = dirname(__DIR__);
		$simplesamlphp = $root.'/vendor/simplesamlphp/simplesamlphp';

		// Creates config if does not exist
		if(!file_exists($root.'/config/config.php')) {
			copy($root . '/config/config-template.php', $root . '/config/config.php');
			echo "Please configure config/config.php\n";
		}

		// Creates runtime dir
		if(!file_exists($root.'/runtime')) {
			mkdir($root.'/runtime');
		}

		// mdx cache dir
		if(!file_exists($root.'/runtime/mdx-cache')) {
			echo "Creating mdx-cache directory\n";
			mkdir($root.'/runtime/mdx-cache', 0770);
		}
		// Copies design module
		static::rcopy($root.'/simplesamlphp/modules/ptedesign', $simplesamlphp.'/modules/design');
		// Copies oid-href.php and href-oid.php to simplesamlphp/attributemap
		static::rcopy($root.'/simplesamlphp/attributemap', $simplesamlphp.'/attributemap');

	}

	/**
	 * Deletes multiple files recursively
	 *
	 * @param string $dir -- directory name or file
	 */
	public static function runlink($dir) {
		if (is_dir($dir)) {
			foreach (scandir($dir) as $file) {
				if ($file != "." && $file != "..")
					self::runlink("$dir/$file");
			}
		}
		else if(file_exists($dir)) {
			unlink($dir);
		}
	}

	/**
	 * copies multiple files from source to destination directory
	 *
	 * @param string $src -- source directory or file
	 * @param string $dst -- destination directory or file
	 * @param bool $overwrite
	 */
	public static function rcopy($src, $dst, $overwrite=false) {
		if (is_dir($src)) {
			if (!file_exists($dst)) mkdir($dst);
			$files = scandir($src);
			foreach ($files as $file) {
				if ($file != "." && $file != "..")
					self::rcopy("$src/$file", "$dst/$file");
			}
		}
		else if(file_exists($src) && !file_exists($dst) || $overwrite) {
			echo "Copying to $dst\n";
			copy($src, $dst);
		}
	}


	/**
	 * Calculates relative path to $to based on $from
	 *
	 * If a folder name is provided as 'to', it must be ended by '/'
	 *
	 * @author http://stackoverflow.com/users/208809/gordon
	 * @param string $from -- absolute basepath
	 * @param string $to -- absolute path to where
	 * @return string -- the relative to $to
	 */
	static function getRelativePath($from, $to) {
		#$xto = $to; $xfrom=$from;
		// some compatibility fixes for Windows paths
		$from = is_dir($from) ? rtrim($from, '\/') . '/' : $from;
		$to   = is_dir($to)   ? rtrim($to, '\/') . '/'   : $to;
		$from = str_replace('\\', '/', $from);
		$to   = str_replace('\\', '/', $to);

		$from     = explode('/', $from);
		$to       = explode('/', $to);
		$relPath  = $to;

		foreach($from as $depth => $dir) {
			// find first non-matching dir
			//if(!array_key_exists($depth, $to)) throw new Exception('Invalid to: '.$xto);
			if(array_key_exists($depth, $to) && $dir === $to[$depth]) {
				// ignore this directory
				array_shift($relPath);
			} else {
				// get number of remaining dirs to $from
				$remaining = count($from) - $depth;
				if($remaining > 1) {
					// add traversals up to first matching dir
					$padLength = (count($relPath) + $remaining - 1) * -1;
					$relPath = array_pad($relPath, $padLength, '..');
					break;
				} else {
					if(array_key_exists(0, $relPath)) $relPath[0] = './' . $relPath[0];
				}
			}
		}
		return implode('/', $relPath);
	}
}
