SP-side TFA demo
================

A demo application with included SimpleSAMLphp SP with integrated two-factor authentication feature, design.
The basic authentication source is EduID.

Compact, composer-based, docker-compatible, easy-to-deploy package.

Uses via composer:
- complete simplesamlphp SP configuration
- design module (Organizational design, extra attribute definitions)
- authtfaga module (2f authentication) optional (switch on for demonstration)

Local Installation
-------------------
1. `$ git clone`
2. (optional) `$ git pull on /modules/*`
3. `$ composer install`
4. Copy your preferred certificate files (if exist) into cert dir or create certificate manually if it has been failed during installation
5. Update environment variables (e.g. in. apache config file) as needed.
6. Make `/web` directory web-accessible on configured hostnames (see `APP_IDP_HOST...` variables).
7. Create `/simplesaml` alias for virtual host to simplesamlphp/www directory as well. 
   Use config/apache-config-template.conf as template to set apache or other environment 

Docker Installation
-------------------
### Creating docker image
0. See local installation steps 1-4
7. `$ docker build -t uhi67/sptfademo:1.0 -t uhi67/sptfademo .`

### Uploading to docker registry
- docker tag uhi67/sptfademo:1.0 registry.../uhi67/sptfademo
- docker push registry.../uhi67/sptfademo

### Deploying docker image in swarm
8. Create `docker-compose.yml` based on `docker-compose-template.yml`
9. Set up environmant variables, ports, etc
10. Set up reverse proxy
11. Deploy `docker-compose.yml` in your swarm environment.

You may place your certificate files in `simplesaml/cert`.
If no certificate file exists in image, it will be generated during container initialization.

Environment variables
---------------------

### php container

variable name | usage
--------------|-------
APP_HTTPS | set to on if you use https *and* ssl-terminating reverse proxy
APP_TECHNICALCONTACT_NAME | technical contact in simplesaml main config. Appears in auto-generated metadata
APP_TECHNICALCONTACT_EMAIL | technical contact in simplesaml main config. Appears in auto-generated metadata
APP_SECRET_SALT | secret key for cookie validation. Use any random, unique string 
APP_ADMIN_PASSWORD | Admin password for simpleSAMLphp admin functions
APP_AUTHSOURCE | SP authentication source used for user validation, *primary* for local/JSON test users, *primary-tfa* for combined with OTP second factor
APP_IDP | primary IdP (Entity id)
APP_DISCO | primary discovery
APP_PTE_COLOR | header bar color for login page
APP_PTE_TITLE | main title for login page -- translation is not supported
APP_PTE_PROMPT | Subtitle/prompt for login page -- translation is not supported
APP_PTE_HELP_TEXT | Helper text for login page -- translation is not supported


### customizing other 

SimpleSAMLphp configuration files are under `simplesamlphp`. 

Design module is in the `simplesamlphp/module/ptedesign` directory, no external VCS source. Change it directly for design changes.

Usage
-----
Test the simplesamlphp installation with 'http://sptfademo.test/simplesaml' (use your configured base url)

Test the functionality with the login button on the start page. 

The root application itself demonstrates usage from external applications.

In this simple TFA demo, the keys of users are stored inside the docker container, so every restart of the service resets the database.
