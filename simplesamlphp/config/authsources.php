<?php

use uhi67\envhelper\EnvHelper;

require_once dirname(dirname(__DIR__)).'/vendor/uhi67/envhelper/src/EnvHelper.php';

/** @noinspection PhpUnhandledExceptionInspection */
$idp = EnvHelper::getEnv('idp', null);
/** @noinspection PhpUnhandledExceptionInspection */
$disco = EnvHelper::getEnv('disco', null);

/** @noinspection PhpUnhandledExceptionInspection */
$config = array(

	// This is a authentication source which handles admin authentication.
	'admin' => array(
		// The default is to use core:AdminPassword, but it can be replaced with
		// any authentication source.

		'core:AdminPassword',
	),

	'localtest' => array(
		'exampleauth:UserPass',
		'local1:local1' => array(
			'uid' => array('local1'),
			'displayName' => 'Local Test One',
			'schacHomeOrganization' => 'pte.hu',
			'eduPersonAffiliation' => array('member', 'employee'),
			'eduPersonPrincipalName' => 'local1@testidp.test',
			'mail' => 'local1@testidp.test',
			'niifPersonOrgID' => array('LOTE1AAA'),
		),
	),

	// This is the combined 2F authentication source. First calls 'primary' and after the OTP authenticator.
	'tfa' => array(
		'authtfaga:authtfaga',

		'db.dsn' => 'sqlite:'.dirname(dirname(__DIR__)).'/runtime/authtf.sq3',
		'mainAuthSource' => 'primary',
		'uidField' => 'uid', // a raw attribute provided by primary data source
		'totpIssuer' => $_SERVER['HTTP_HOST'],
	),

	// Primary login
	'primary' => array(
		'saml:SP',

		// The entity ID of this SP.
		// Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
		'entityID' => null,

		// The entity ID of the IdP this should SP should contact.
		// Can be NULL/unset, in which case the user will be shown a list of available IdPs.
		'idp' => $idp,
		'discoURL' => $disco,

		'signature.algorithm' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',
		'authproc' => array(
			91 => array('class' => 'core:AttributeMap', 'oid2name'),
		),
		'privatekey' => 'testsp.pem',
		'certificate' => 'testsp.crt',
	),
);
