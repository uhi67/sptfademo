<?php

use uhi67\helper\EnvHelper;

/** @noinspection PhpUnhandledExceptionInspection */
$config = array(
   'sets' => array(
        'pte-test' => array(
            'cron'      => array('hourly'),
            'sources'   => array(
                array(
                    'src' => EnvHelper::getEnv('rrMetaSource', 'http://rr.test/samlres/metadata/pte-test.xml'),
                ),
            ),
            'expireAfter'  => 60*60*24*4, // Maximum 4 days cache time.
            'outputDir'    => dirname(__DIR__).'/metadata/pte-test/',
            'outputFormat' => 'flatfile',
        ),

   ),
);
