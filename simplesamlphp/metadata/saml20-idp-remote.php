<?php
/*
 * PTE IdP
 */
$metadata['https://idp.pte.hu/saml2/idp/metadata.php'] = array(
	'name' => array(
		'hu' => 'Pécsi Tudományegyetem',
		'en' => 'Pecs University, Hungary'
	),
	'description'          => 'Here you can login with your account on PTE.',

	'SingleSignOnService'  => 'https://idp.pte.hu/saml/saml2/idp/SSOService.php',
	'SingleLogoutService'  => 'https://idp.pte.hu/saml/saml2/idp/SingleLogoutService.php',
	'certFingerprint'      => '91:FF:FB:8B:D1:35:1E:2E:71:05:2D:EB:03:29:BC:26:91:63:E1:AC'
);

/*
 * PTE teszt IdP
 */
$metadata['http://testidp.pte.hu/saml2/idp/metadata.php'] = array(
	'name' => array(
		'hu' => 'Pécsi Tudományegyetem teszt',
		'en' => 'Pecs University, Hungary, Test'
	),
	'description'          => array(
		'hu' => 'Itt a PTE teszt IdP-n meghatározott tesztuserekkel tud bejelentkezni',
		'en' => 'Here you can login with a test account on PTE.',
	),
	'SingleSignOnService'  => 'http://testidp.pte.hu/saml/saml2/idp/SSOService.php',
	'SingleLogoutService'  => 'http://testidp.pte.hu/saml/saml2/idp/SingleLogoutService.php',
	'certFingerprint'      => '8E:45:21:0D:16:67:AF:EC:3C:D4:EA:73:9C:48:30:E7:E2:9D:6B:B3'
);
