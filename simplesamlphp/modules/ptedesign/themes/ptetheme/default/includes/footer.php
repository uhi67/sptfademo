<?php

if (!empty($this->data['htmlinject']['htmlContentPost'])) {
    foreach ($this->data['htmlinject']['htmlContentPost'] AS $c) {
        echo $c;
    }
}


?>
</div>
</div><!-- #content -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-xs-12-inner-text">
                <div class="footer-text">
                    <img class="img-responsive footer-logo"
                         src="<?= SimpleSAML\Module::getModuleURL('design/img/PTE_emblema_90_90.png') ?>" width="70">
                    <p>PTE Kancellária Informatikai Igazgatóság<br>
                        H-7633 Pécs, Szántó Kovács János u. 1/B.<br>
                        +36 72 501-500/ 36006 | <a href="mailto:sd@pte.hu">sd@pte.hu</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
