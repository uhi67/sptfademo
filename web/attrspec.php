<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >
<html>
<head>
	<title>PTE Test IdP</title>
	<link rel="stylesheet" type="text/css" href="css/idp.css">
	<script type="text/javascript" src="jQuery/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="Showdown/dist/showdown.min.js"></script>
	<script type="text/javascript" src="Showdown/Showdown.js"></script>
</head>
<?php
	include 'saml.php';
?>
<body>
<?php
include 'header.php';
?>
<div id="content_box">
    <div id="content">
        <div id="content_left">
            <!-- Balsávi tartalom -->
            <p><a href="/">Kezdőlap</a> </p>
			<?php include 'menu.php'; ?>
            <!-- Balsávi tartalom vége -->
        </div>
        <div id="content_main" class="wide">
            <?php if($isAuth): ?>
                <style>
                    td div:nth-of-type(2) { margin-left:16px;}
                    tr td { background-color: #eee; }
                </style>
                <h1>Attributum specifikáció</h1>
                <h2>A PTE Test IdP által előállított attributumok</h2>
                <p>A test IdP a tesztadatforrásban (lokálisan vagy a távoliban) manuálisan rögzített tetszőleges attributumokat adja vissza. A teszt használhatósága érdekében azonban nem árt, ha az attributumok köszönő viszonyban vannak az éles rendszerben használatos attributumok készletével:</p>
                <table>
                    <th>Név/OID</th><th>Érvényesség</th><th>Tartalom</th></tr>
                    <tr>
                        <td>
                            <div><a href="https://wiki.aai.niif.hu/index.php/HREFAttributeSpec#eduPersonPrincipalName">
                                    eduPersonPrincipalName</a></div>
                            <div>1.3.6.1.4.1.5923.1.1.1.6</div>
                        </td>
                        <td>EduID, EduGAIN</td>
                        <td>"<i>uid</i>@pte.hu", ahol <i>uid</i> általában EHA kód, vagy Neptun kód, vagy rövid login név.</td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="https://wiki.aai.niif.hu/index.php/HREFAttributeSpec#eduPersonScopedAffiliation">
                                    eduPersonScopedAffiliation</a></div>
                            <div>1.3.6.1.4.1.5923.1.1.1.9</div>
                        </td>
                        <td>EduID, EduGAIN</td>
                        <td>hallgatók: "student@pte.hu"; dolgozók: "employee@pte.hu" és "staff@pte.hu"; mindenki: "member@pte.hu"; továbbá az affiliate AAI attributum szkópolt értékei.</td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="https://wiki.aai.niif.hu/index.php/HREFAttributeSpec#eduPersonTargetedID">
                                    eduPersonTargetedID</a></div>
                            <div>1.3.6.1.4.1.5923.1.1.1.10</div>
                        </td>
                        <td>EduID, EduGAIN</td>
                        <td>Személyhez kapcsolódó, de személyes adatot nem tartalmazó random állandó azonosító @pte.hu szkóppal. Egyedisége az uid egyediségére alapul.</td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="https://wiki.aai.niif.hu/index.php/HREFAttributeSpec#schacHomeOrganizationType">
                                    schacHomeOrganizationType</a></div>
                            <div>1.3.6.1.4.1.25178.1.2.10</div>
                        </td>
                        <td>EduID, EduGAIN</td>
                        <td>'urn:schac:homeOrganizationType:hu:university'</td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="https://wiki.aai.niif.hu/index.php/HREFAttributeSpec#displayName">
                                    displayName</a></div>
                            <div>2.16.840.1.113730.3.1.241</div>
                        </td>
                        <td>EduID, EduGAIN</td>
                        <td>Teljes név az AAI-ból</td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="https://wiki.aai.niif.hu/index.php/HREFAttributeSpec#email">
                                    mail</a></div>
                            <div>0.9.2342.19200300.100.1.3</div>
                        </td>
                        <td>EduID, EduGAIN</td>
                        <td>E-mail cím az AAI-ból</td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="https://wiki.aai.niif.hu/index.php/HREFAttributeSpec#jpegPhoto">
                                    jpegPhoto</a></div>
                            <div>0.9.2342.19200300.100.1.60</div>
                        </td>
                        <td>EduID/EduGain</td>
                        <td>Kis méretű fotó a felhasználóról JPEG formátumban</td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="https://www.switch.ch/aai/support/documents/attributes/schachomeorganization/">
                                    schacHomeOrganization</a></div>
                            <div>1.3.6.1.4.1.25178.1.2.9</div>
                        </td>
                        <td>EduGAIN</td>
                        <td>'pte.hu'</td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="https://www.internet2.edu/media/medialibrary/2013/09/04/internet2-mace-dir-eduperson-201203.html#eduPersonAffiliation">
                                    eduPersonAffiliation</a></div>
                            <div>1.3.6.1.4.1.5923.1.1.1.1</div>
                        </td>
                        <td>EduGAIN</td>
                        <td>Hallgatók: "student"; dolgozók: "employee" és "staff"; mindenki: "member"; továbbá az affiliate AAI attributum értékei.</td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="https://wiki.aai.niif.hu/index.php/HREFAttributeSpec#niifPersonOrgID">
                                    niifPersonOrgID</a></div>
                            <div>1.3.6.1.4.1.11914.0.1.154</div>
                        </td>
                        <td>PTE</td>
                        <td>Neptun kód az AAI-ból</td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="https://wiki.aai.niif.hu/index.php/HREFAttributeSpec#sn">
                                    sn</a></div>
                            <div>2.5.4.4</div>
                        </td>
                        <td>PTE</td>
                        <td>Vezetéknév az AAI-ból</td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="https://wiki.aai.niif.hu/index.php/HREFAttributeSpec#givenName">
                                    givenName</a></div>
                            <div>2.5.4.42</div>
                        </td>
                        <td>PTE</td>
                        <td>Keresztnév az AAI-ból</td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="https://wiki.aai.niif.hu/index.php/HREFAttributeSpec#ou">
                                    ou</a></div>
                            <div>2.5.4.11</div>
                        </td>
                        <td>PTE</td>
                        <td>Dolgozó szervezeti egysége; Hallgató karai (beleértve a nem aktív képzéshez tartozókat is) </td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="http://www.internet2.edu/media/medialibrary/2013/09/04/internet2-mace-dir-eduperson-200806.html#o">
                                    o</a></div>
                            <div>2.5.4.10</div>
                        </td>
                        <td>PTE</td>
                        <td>['Pécsi Tudományegyetem', 'University of Pécs', 'PTE']</td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="https://wiki.aai.niif.hu/index.php/HREFAttributeSpec#niifEduPersonAttendedCourse">
                                    niifPersonAttendedCourse</a></div>
                            <div>1.3.6.1.4.1.11914.0.1.164</div>
                        </td>
                        <td>PTE</td>
                        <td>Hallgató esetén az aktív képzéseinek megnevezései</td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="https://wiki.aai.niif.hu/index.php/HREFAttributeSpec#niifEduPersonFaculty">
                                    niifEduPersonFaculty</a></div>
                            <div>1.3.6.1.4.1.11914.0.1.160</div>
                        </td>
                        <td>PTE</td>
                        <td>Hallgató esetén az aktív képzéseihez tartozó karok megnevezései</td>
                    </tr>
                    <tr>
                        <td>
                            <div><a href="https://wiki.aai.niif.hu/index.php/HREFAttributeSpec#preferredLanguage">
                                    preferredLanguage</a></div>
                            <div>2.16.840.1.113730.3.1.39</div>
                        </td>
                        <td>egyéb</td>
                        <td>A felhasználó által bejelentkezés során választott nyelv</td>
                    </tr>
                </table>
            <?php else: ?>
                <p>Ön nincs belépve.</p>
            <?php endif ?>
        </div>
    </div>
</div>
</body>
</html>
