<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >
<html>
<head>
	<title>SP TFA DEMO</title>
	<link rel="stylesheet" type="text/css" href="css/idp.css">
	<style type="text/css">
		#content_main ul li {list-style: circle;}
	</style>
</head>
<?php

use uhi67\helper\EnvHelper;

include 'saml.php';
?>
<body>
<?php
	include 'header.php';
?>
    <!-- felső menük helye -->
    <div id="content_box">
      <div id="content">
        <div id="content_left">
            <!-- Balsávi tartalom -->
            <?php include 'menu.php'; ?>
            <!-- Balsávi tartalom vége -->
        </div>
        <div id="content_main" class="wide">
            <!-- tartalom -->
            <h1>Kétfaktoros EduID azonosítási szolgáltatás DEMO</h1>
            <p>Lépjen be EduID azonosítójával!</p>
            <p>A belépést követően első alkalommal regisztrálhatja a második faktor kódját, vagy kell megadnia a kódot.</p>

            <h2>Műszaki részletek érdeklődőknek</h2>
            <p>A kétfaktoros azonosítás az SP oldali SimpleSAMLphp beléptetésbe van integrálva, így bármely IdP-n át belépő felhasználóra érvényes.</p>
            <p>A felhasználói mobil eszközön Google Authenticator vagy azzal kompatibilis alkalmazás használható.</p>
            <p>A demo forráskódja <a href="https://bitbucket.org/uhi67/sptfademo">a Bitbucketen</a></p>
            <p>Az alkalmazott modul: <a href="https://github.com/uhi67/simplesamlphp-module-authtfaga">"uhi67/simplesamlphp-module-authtfaga"</a></p>
            <p>Jelen demo egy PTE-mintájú design témát használ (design modul), ezt más intézményekben le kell cserélni a megfelelőre.</p>
            <h2>Adatkezelés</h2>
            <p>Jelen alkalmazás az ön egyes személyes attributumait kizárólag az önnek való megmutatás céljából kéri el az azonosító rendszertől.
                Adatai itt sem további tárolásra, sem feldolgozásra vagy harmadik fél vagy rendszer felé továbbításra nem kerülnek.
                A rendszer működése céljából tároljuk az ön személyhez nem kapcsolható célzott azonosítóját a második faktoros kulccsal összekapcsolva.
                Ezt az adatot legfeljebb egy évig tároljuk.
            </p>
        </div>
       	<div id="content_right">
       		<!-- Jobbsávi tartalom -->
        </div>
        <div class="cboth"></div>
      </div>
    </div>
    <div id="footer_box">
      <div id="footer">
        <a href="http://www.pte.hu" class="footer_logo" title="Kezdőlap"><img src="img/footer_logo.gif" alt="" /></a>
        <address><span class="addr_title">Pécsi Tudományegyetem</span><br />H-7633 Pécs, Szántó Kovács János u. 1/B.<br />+36 72 501-500 | <a href="mailto:info@pte.hu">info@pte.hu</a> | <a href="#">RSS</a></address>
      </div>
    </div>

</body>
</html>

