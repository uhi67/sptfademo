<h2>Saját adatok</h2>
<ul>
	<?php if($isAuth): ?>
		<li>Belépve mint <?= $attributes['displayName'][0] ?></li>
		<li><a href="?logout">Kilépés</a></li>
	<?php else : ?>
		<li><a href="?login" title="EduID Belépés"><div class="eduid-login">Belépés</div></a></li>
	<?php endif ?>
	<li><a href="showattributes.php">Saját attributumok</a></li>
</ul>

<?php if($isIIG): ?>
<h2>Adminisztráció</h2>
<ul>
	<li><a href="attrspec.php">Attributumok specifikációja</a></li>
	<li><a href="simplesaml">IdP adminisztrációs felület</a></li>
	<li><a href="https://rr.pte.hu">PTE Resource Registry</a></li>
	<li><a href="spsetup.php">SP telepítési útmutató</a></li>
	<li><a href="http://iigredmine.pte.hu/projects/idp/wiki/Hibaelh%C3%A1r%C3%ADt%C3%B3">Belépési hiba elhárítási útmutató</a></li>
</ul>
<?php endif ?>

<h2>Föderáció</h2>
<ul>
	<li><a target="_blank" href="http://www.eduid.hu/">Az EduID</a></li>
	<li><a target="_blank" href="https://wiki.aai.niif.hu/index.php?title=AAI">HREF tudásbázis</a></li>
	<li><a target="_blank" href="https://rr.aai.niif.hu/">HREF Resource registry</a></li>
</ul>
