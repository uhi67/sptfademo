<?php
ini_set('display_errors', 'stdout');
require_once dirname(__DIR__).'/vendor/autoload.php';

use uhi67\helper\EnvHelper;

/** @noinspection PhpUnhandledExceptionInspection */
$url = EnvHelper::getEnv('rrauth_url');
/** @noinspection PhpUnhandledExceptionInspection */
$key = EnvHelper::getEnv('rrauth_key');
$username = 'test1';
$password = 'test1';

$c = curl_init();
curl_setopt($c, CURLOPT_URL, $url);
curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 5);
$params = array('secret'=>hash('sha256', $data = $username."\n".$password."\n".$key));
curl_setopt($c, CURLOPT_POST, true);
curl_setopt($c, CURLOPT_POSTFIELDS, $params);
$response = curl_exec($c);
$info = curl_getinfo($c, CURLINFO_HTTP_CODE );
curl_close($c);
SimpleSAML\Logger::debug($url . ' response: '. $response);

echo "<div>Calling '$url' with data '".print_r($params, true)."'</div>";

$result = json_decode($response, true);

echo "<div>Result is '" . ($response ? $response : print_r($info, true)) ."'</div>";
